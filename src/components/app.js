import React from 'react'

import 'bootstrap/dist/css/bootstrap.css';  
import Addmoney from '../containers/Addmoney';
import Withdraw from '../containers/Withdraw';

const App = () => (
  <div>
    <Addmoney />
    <Withdraw />
  </div>
)

export default App
