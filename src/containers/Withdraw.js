import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { Input, Row, Col, Card, Button } from 'reactstrap'
import { isString } from 'util';
let totalMoney = 0
let amountofbank100 = 0
let amountofbank500 = 0
let amountofbank1000 = 0
let key = ''
class Withdraw extends Component {

  constructor(props) {
    super(props)
    this.state = {
      withdrawalAmount: 0
    }
  }

  componentWillMount() {
    console.log(this.props.todos)
  }
  handleRemoveItem = (key) => {
    const { firebase } = this.props

    firebase.remove(`/atm/${key}`).then(() => {
      alert("ลบรายการงานเรียบร้อย")
    }).catch(error => {
      alert(`ไม่สามารถลบรายการงาน (${error.message})`)
    })
  }

  handleItemClick = (key, value) => {
    const { firebase } = this.props

    firebase.update(`/atm/${key}`, { done: !value.done }).then(() => {
      alert("อัพเดทรายการงานเรียบร้อย")
    }).catch(error => {
      alert(`ไม่สามารถอัพเดทรายการงาน (${error.message})`)
    })
  }

  renderList(todos) {
    return todos.map(({ key, value }) => {
      // console.log(value)
      return (
        <li key={key}>
          <button onClick={() => this.handleRemoveItem(key)}>ลบ</button> | <span onClick={() => this.handleItemClick(key, value.money100)}>{value.money100}</span>
        </li>
      )
    })
  }

  display(todos) {
    let M_100 = 0
    let M_500 = 0
    let M_1000 = 0
    amountofbank100 = 0
    amountofbank500 = 0
    amountofbank1000 = 0
    if (todos && todos[0].value) {
      amountofbank100 = todos[0].value.money100
      amountofbank500 = todos[0].value.money500
      amountofbank1000 = todos[0].value.money1000

      M_100 = todos[0].value.money100 * 100
      M_500 = todos[0].value.money500 * 500
      M_1000 = todos[0].value.money1000 * 1000
    }
    if (todos && todos[0].key) {
      key = todos[0].key
    }

    return <div>

      <Col md={12} xs={12}>
        <div style={{ color: 'green', fontWeight: 'bold' }}>ยอดเงินทั้งหมด{'    '}{M_100 + M_500 + M_1000}{'  '}บาท</div>
      </Col>

      <Row style={{ paddingTop: 40 }}>
        <Col md={4}>
          <div style={{ color: 'blue' }}>จำนวนธนบัตรใบละ 100 เหลืออยู่ [{todos[0].value.money100}]</div>
        </Col>
        <Col md={4}>
          <div style={{ color: 'blue' }}>จำนวนธนบัตรใบละ 500 เหลืออยู่ [{todos[0].value.money500}]</div>
        </Col>
        <Col md={4}>
          <div style={{ color: 'blue' }}>จำนวนธนบัตรใบละ 1000 เหลืออยู่ [{todos[0].value.money1000}]</div>
        </Col>
      </Row>
    </div>
  }

  process() {
    const { withdrawalAmount } = this.state
    let amount100 = 0
    let amount500 = 0
    let amount1000 = 0
    let save = 0
    // console.log('withdrawal : ' + withdrawalAmount)
    // console.log('amoun of bank : ' + amountofbank100)
    if (withdrawalAmount >= 1000 && amountofbank1000 != 0) {
      // console.log('1')
      amount1000 = parseInt(withdrawalAmount / 1000)
      // console.log(amount1000)
      if (amountofbank1000 >= amount1000) {
        save = withdrawalAmount % 1000
        if (withdrawalAmount % 1000 == 0) {
          // console.log(save)
          // console.log('x1000')
          alert('ใช้ธนบัตรใบละ 1000 บาท จำนวน ' + amount1000 + ' ใบ')
          this.updateData(key, amount100, amount500, amount1000)
        }

      }
      if (withdrawalAmount % 1000 != 0 && amountofbank500 >= (withdrawalAmount % 1000) / 500) {

        save = save % 500
        amount500 = parseInt((withdrawalAmount % 1000) / 500)
        // console.log(save)
        // console.log(amount500)
        if (save == 0) {
          console.log('x1000=>500')
          alert('ใช้ธนบัตรใบละ 1000 บาท จำนวน ' + amount1000 + ' ใบ   ' + 'ใช้ธนบัตรใบละ 500 บาท จำนวน ' + amount500 + ' ใบ   ')
          this.updateData(key, amount100, amount500, amount1000)
        }


      }
      // console.log(save)
      if (save % 100 == 0 && amountofbank100 >= save / 100) {
        amount100 = save / 100
        alert(('ใช้ธนบัตรใบละ 1000 บาท จำนวน ' + amount1000 + ' ใบ   ' + 'ใช้ธนบัตรใบละ 500 บาท จำนวน ' + amount500 + ' ใบ   ' + 'ใช้ธนบัตรใบละ 100 บาท จำนวน ' + amount100 + ' ใบ   '))
        this.updateData(key, amount100, amount500, amount1000)
      }
      else {
        alert('จำนวนธนบัตรไม่เพียงพอกรุณาทำรายการใหม่ภายหลัง')
      }
    }
    else if (withdrawalAmount >= 500 && amountofbank500 != 0) {
      // console.log('2')
      amount500 = parseInt(withdrawalAmount / 500)
      // console.log(amount500)

      if (amountofbank500 >= amount500 && withdrawalAmount % 500 == 0) {
        // console.log('x500')
        alert('ใช้ธนบัตรใบละ 500 บาท จำนวน ' + amount500 + ' ใบ')
        this.updateData(key, amount100, amount500, amount1000)
      }
      // console.log((withdrawalAmount % 500) / 100)
      if (withdrawalAmount % 500 != 0 && (withdrawalAmount % 500) % 100 == 0 && amountofbank100 >= (withdrawalAmount % 500) / 100) {
        alert('5555555555555s')
        amount100 = (withdrawalAmount % 500) / 100
        // console.log('x500=>100')
        alert('ใช้ธนบัตรใบละ 500 บาท จำนวน ' + amount500 + ' ใบ   ' + 'ใช้ธนบัตรใบละ 100 บาท จำนวน ' + amount100 + ' ใบ   ')
        this.updateData(key, amount100, amount500, amount1000)
      }
      alert('จำนวนธนบัตรไม่เพียงพอกรุณาทำรายการใหม่ภายหลัง')
    }
    else if (withdrawalAmount >= 100 && amountofbank100 != 0) {
      // console.log('3')
      amount100 = withdrawalAmount / 100
      let mod100 = withdrawalAmount % 100
      // console.log(amount100 > amountofbank100)
      if (amountofbank100 >= amount100 && mod100 == 0) {
        // console.log('x100')
        alert('ใช้ธนบัตรใบละ 100 บาท จำนวน ' + amount100 + ' ใบ')
        this.updateData(key, amount100, amount500, amount1000)
      }
      else {
        // console.log('e100')
        alert('จำนวนธนบัตรไม่เพียงพอกรุณาทำรายการใหม่ภายหลัง')
      }
    }
    else {
      // console.log('5')
      alert('จำนวนธนบัตรไม่เพียงพอกรุณาทำรายการใหม่ภายหลัง')
    }
  }

  updateData(key, amount100, amount500, amount1000) {

    //console.log(key+'/'+amount100+'/'+amount500+'/'+amount1000+'/')
    const { firebase } = this.props

    let m_100 = amountofbank100 - amount100
    let m_500 = amountofbank500 - amount500
    let m_1000 = amountofbank1000 - amount1000

    firebase.update(`/atm/${key}`, { money100: m_100, money500: m_500, money1000: m_1000 }).then(() => {
      alert("ทำรายการสำเร็จ")
    }).catch(error => {
      alert(`การทำรายการล้มเหลว (${error.message})`)
    })
  }
  render() {

    const { todos } = this.props

    console.log(todos)
    if (!isLoaded(todos)) {
      return <div style={{ textAlign: 'center', paddingTop: 50 }}>'กำลังโหลด...'</div>
    }

    if (isEmpty(todos)) {
      return 'รายการงานว่าง'
    }

    return (
      <div style={{ textAlign: 'center', paddingTop: 50, display: 'flex', justifyContent: 'center' }}>
        <Card style={{ marginTop: 50, padding: 10, display: 'inline-flex', maxWidth: 500 }}>
          {this.display(todos)}
          <Row style={{ paddingTop: 40 }}>
            <Col md={12} xs={12}>
              <Input type='number'
                placeholder='จำนวนเงินที่ต้องการถอน'
                onChange={(event) => this.setState({ withdrawalAmount: event.target.value })} />
            </Col>

          </Row>
          <Row style={{ justifyContent: 'center',paddingTop:10 }}>
            <Button color='primary' onClick={() => this.process()}>ยืนยัน</Button>
          </Row>
          {/* {this.renderList(todos)} */}
        </Card>
      </div>


    )
  }
}

function mapStateToProps({ firebase }) {
  return {
    todos: firebase.ordered.atm
  }
}

const enhance = compose(firebaseConnect([
  { path: '/atm', queryParams: ['orderByChild=timestamp'] }
]), connect(mapStateToProps))

export default enhance(Withdraw)
