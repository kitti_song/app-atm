import React, { Component } from 'react'
import { withFirebase, firebaseConnect, isEmpty } from 'react-redux-firebase'
import { Input, Label, Button, Row, Col, Card } from 'reactstrap'

import { connect } from 'react-redux'
import { compose } from 'redux'


class Addmoney extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value100: 0,
      value500: 0,
      value1000: 0
    }
    //this.state = { content: '' }
  }

  // handleChange = (event) => {
  //   this.setState({ content: event.target.value })
  // }

  // handleKeyPress = (event) => {
  //   const { firebase } = this.props
  //   const { content } = this.state

  //   if (event.key === 'Enter') {
  //     firebase.push('/todos', {
  //       content: content,
  //       done: false,
  //       timestamp: firebase.database.ServerValue.TIMESTAMP
  //     }).then(() => {
  //       alert("เพิ่มรายการงานเรียบร้อย")
  //       this.setState({ content: '' })
  //     }).catch(error => {
  //       alert(`ไม่สามารถเพิ่มรายการงาน (${error.message})`)
  //     })
  //   }
  // }
  process() {
    const { todos } = this.props
    const { firebase } = this.props
    const { value100, value500, value1000 } = this.state
    if (isEmpty(this.props.todos)) {
      firebase.push('/atm', {
        money100: value100,
        money500: value500,
        money1000: value1000,
        timestamp: firebase.database.ServerValue.TIMESTAMP
      }).then(() => {
        alert("ฝากเงินเรียบร้อย")
        this.setState({ content: '' })
      }).catch(error => {
        alert(`ไม่สามารถฝากเงินได้ (${error.message})`)
      })
    }
    else {
      let key = ''
      const { firebase } = this.props
      if (todos && todos[0].value) {
        key = todos[0].key
        let m_100 = parseInt(todos[0].value.money100) + parseInt(value100)
        let m_500 = parseInt(todos[0].value.money500) + parseInt(value500)
        let m_1000 = parseInt(todos[0].value.money1000) + parseInt(value1000)
        console.log(m_100)

        firebase.update(`/atm/${key}`, { money100: m_100, money500: m_500, money1000: m_1000 }).then(() => {
          alert("อัพเดทรายการงานเรียบร้อย")
        }).catch(error => {
          alert(`ไม่สามารถอัพเดทรายการงาน (${error.message})`)
        })
      }


    }


  }

  render() {
    return (
      <div style={{ textAlign: 'center' }}>
        < Card style={{ marginTop: 50, padding: 10, display: 'inline-flex', maxWidth: 500 }}>
          <div>ฝากเงิน</div>
          <Row style={{ paddingBottom: 10, paddingTop: 20 }}>
            <Col md={6}>
              <div>
                <Input type='checkbox' /> ธนบัตร 100 บาท
                            </div>


            </Col>
            <Col md={6}>
              <Input type='number' onChange={(event) => this.setState({ value100: event.target.value })} placeholder='ใส่จำนวนธนบัตร' />
            </Col>

            <Col md={6}>
              <div>
                <Input type='checkbox' /> ธนบัตร 500 บาท
                            </div>


            </Col>
            <Col md={6}>
              <Input type='number' onChange={(event) => this.setState({ value500: event.target.value })} placeholder='ใส่จำนวนธนบัตร' />
            </Col>

            <Col md={6}>
              <div>
                <Input type='checkbox' /> ธนบัตร 1000 บาท
                            </div>


            </Col>
            <Col md={6}>
              <Input type='number' onChange={(event) => this.setState({ value1000: event.target.value })} placeholder='ใส่จำนวนธนบัตร' />
            </Col>
          </Row>
          <Row style={{ justifyContent: 'center' }}>
            <Button color='primary' onClick={() => this.process()}>ยืนยัน</Button>
          </Row>

        </Card >
        {/* <input
          type='text'
          onChange={this.handleChange}
          onKeyPress={this.handleKeyPress}
          value={this.state.content}
          autoFocus
        /> */}
      </div>
    )
  }
}

function mapStateToProps({ firebase }) {
  return {
    todos: firebase.ordered.atm
  }
}

const enhance = compose(firebaseConnect([
  { path: '/atm', queryParams: ['orderByChild=timestamp'] }
]), connect(mapStateToProps))

export default enhance(withFirebase(Addmoney))
